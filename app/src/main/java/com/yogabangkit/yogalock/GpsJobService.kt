package com.yogabangkit.yogalock

import android.app.job.JobParameters
import android.app.job.JobService
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.os.Bundle
import com.yogabangkit.yogalock.email.GmailSender
import com.yogabangkit.yogalock.util.UserPreference
import kotlinx.coroutines.experimental.CommonPool
import kotlinx.coroutines.experimental.launch
import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.info
import org.jetbrains.anko.locationManager

class GpsJobService : JobService(), AnkoLogger, LocationListener {
    private val emailTo = "yogaabangkit@gmail.com"

    override fun onLocationChanged(location: Location?) {
        location?.let {
            info("latitude: ${it.latitude} longitude: ${it.longitude}")
            launch(CommonPool) {
                gmailSender.sendMail(
                        "send coordinate",
                        "latitude: ${location.latitude}, longitude: ${location.longitude}",
                        emailSender,
                        emailTo
                )
                info("message sent $emailTo")
            }
        }
    }

    override fun onStatusChanged(p0: String?, p1: Int, p2: Bundle?) {
    }

    override fun onProviderEnabled(p0: String?) {
    }

    override fun onProviderDisabled(p0: String?) {
    }

    private val emailSender = "yogaabangkit@gmail.com"
    private var lm: LocationManager? = null

    private val gmailSender = GmailSender("yogaabangkit@gmail.com", "pramonos")

    override fun onStopJob(params: JobParameters?): Boolean {
        lm?.removeUpdates(this)
        lm = null
        return false
    }

    override fun onStartJob(params: JobParameters?): Boolean {
        info("gps job started")
        lm = applicationContext.locationManager
        lm?.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0f, this)

        return true
    }
}