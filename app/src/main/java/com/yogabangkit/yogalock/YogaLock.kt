package com.yogabangkit.yogalock

import android.app.Application
import com.marcinmoskala.kotlinpreferences.PreferenceHolder

class YogaLock : Application() {

    override fun onCreate() {
        super.onCreate()
        PreferenceHolder.setContext(applicationContext)
    }
}