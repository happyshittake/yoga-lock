package com.yogabangkit.yogalock

import android.Manifest
import android.accounts.AccountManager
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.provider.Settings
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import com.google.android.gms.auth.GoogleAuthUtil
import com.google.android.gms.common.AccountPicker
import com.yogabangkit.yogalock.util.UserPreference
import kotlinx.android.synthetic.main.activity_enable.*
import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.info
import org.jetbrains.anko.intentFor
import org.jetbrains.anko.toast

class EnableActivity : AppCompatActivity(), AnkoLogger {

    private val DRAW_PERMISSION_REQUEST = 1
    private val GET_EMAIL_ADDRESS = 2
    private val REQUEST_PERMISSION_LOCATION = 3

    companion object {
        val GPS_GET = 67
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_enable)

        enableLock.setOnClickListener {
            info("sanity check clicked")
            if (!checkPermission()) {
                getPermissionToRun()
            } else {
                startApp()
            }
        }
    }

    private fun startApp() {
        if (UserPreference.email == null) {
            info("neeed email")
            getEmail()
        } else {
            startServices()
        }
    }

    private fun getEmail() {
        val intent = AccountPicker.newChooseAccountIntent(
                null,
                null,
                arrayOf(GoogleAuthUtil.GOOGLE_ACCOUNT_TYPE),
                false,
                null,
                null,
                null,
                null
        )
        startActivityForResult(intent, GET_EMAIL_ADDRESS)
    }

    private fun startServices() {
        startService(intentFor<LocalService>())
        toast("service has been started")
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        when (requestCode) {
            DRAW_PERMISSION_REQUEST -> {
                if (checkPermission()) {
                    startApp()
                } else {
                    toast("anda belum menyalakan fitur draw over other app")
                }
            }
            GET_EMAIL_ADDRESS -> {
                val emailaddress = data?.getStringExtra(AccountManager.KEY_ACCOUNT_NAME)
                emailaddress?.let {
                    UserPreference.email = it
                }
                startServices()
            }
        }

        super.onActivityResult(requestCode, resultCode, data)
    }


    private fun checkPermission(): Boolean = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
        Settings.canDrawOverlays(this) else true

    private fun getPermissionToRun() {
        info("need permission")
        if (!checkPermission()) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                info("tess")
                startActivityForResult(Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION), DRAW_PERMISSION_REQUEST)
                return
            }
        }

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(
                    this,
                    arrayOf(Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION),
                    REQUEST_PERMISSION_LOCATION
            )
        }
    }
}
