package com.yogabangkit.yogalock

import android.app.Service
import android.content.Intent
import android.content.IntentFilter
import android.os.IBinder
import org.jetbrains.anko.AnkoLogger

class LocalService : Service(), AnkoLogger {
    private val receiver: LocalBroadcastReceiver = LocalBroadcastReceiver()
    private var rebootIntent: Intent? = null

    override fun onBind(intent: Intent?): IBinder? {
        return null
    }

    override fun onCreate() {
        val filter = IntentFilter()
        filter.addAction(Intent.ACTION_SCREEN_ON)
        filter.addAction(Intent.ACTION_SCREEN_OFF)
        filter.addAction(Intent.ACTION_USER_FOREGROUND)
        filter.addAction(Intent.ACTION_USER_BACKGROUND)

        registerReceiver(receiver, filter)

//        val keyboardManager = getSystemService(Context.KEYGUARD_SERVICE) as KeyguardManager
//        val keyguardLock = keyboardManager.newKeyguardLock("keyguardlock")
//        keyguardLock.disableKeyguard()

        super.onCreate()
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        rebootIntent = intent
        return super.onStartCommand(intent, flags, startId)
    }

    override fun onDestroy() {
        unregisterReceiver(receiver)
        rebootIntent?.let {
            startService(rebootIntent)
        }

        super.onDestroy()
    }
}
