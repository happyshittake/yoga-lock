package com.yogabangkit.yogalock

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.Button
import com.yogabangkit.yogalock.util.LockUtil
import org.jetbrains.anko.find
import org.jetbrains.anko.intentFor
import org.jetbrains.anko.newTask
import org.jetbrains.anko.toast

class MainActivity : AppCompatActivity() {
    private var lockView: View? = null
    private var lockUtil: LockUtil? = null

    companion object Factory {
        var isShown = false
        var instance : MainActivity? = null
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        isShown = true
        instance = this
        lockView = View.inflate(this, R.layout.activity_main, null)
        lockUtil = LockUtil.create(this)
        lockUtil?.lockView = lockView
        lockUtil?.lock()

        lockView?.find<Button>(R.id.unlockButton)?.setOnClickListener {
            lockUtil?.unlock()
            toast("unlocked...")
            startActivity(intentFor<FlingScreenLockActivity>().newTask())
            finish()
        }
    }

    override fun onDestroy() {
        isShown = false
        instance = null
        super.onDestroy()
    }
}
