package com.yogabangkit.yogalock

import android.app.job.JobParameters
import android.app.job.JobService
import com.yogabangkit.yogalock.util.TerminalUtil

class ResetJobService : JobService() {
    override fun onStopJob(p0: JobParameters?): Boolean = false

    override fun onStartJob(p0: JobParameters?): Boolean {
        TerminalUtil.hardReset()

        return false
    }
}