package com.yogabangkit.yogalock.util

import android.content.Context
import android.location.Location
import android.location.LocationManager
import org.jetbrains.anko.locationManager

object GpsUtil {

    suspend fun getLocation(context: Context): Location {
        val lm = context.locationManager
        return lm.getLastKnownLocation(LocationManager.NETWORK_PROVIDER)
    }
}