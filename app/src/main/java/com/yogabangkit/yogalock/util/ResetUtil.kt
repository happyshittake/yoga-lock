package com.yogabangkit.yogalock.util

import android.app.job.JobInfo
import android.app.job.JobScheduler
import android.content.ComponentName
import android.content.Context
import com.yogabangkit.yogalock.ResetJobService
import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.info

class ResetUtil(val context: Context) : AnkoLogger {
    private val jobScheduler = context.getSystemService(Context.JOB_SCHEDULER_SERVICE) as JobScheduler

    companion object {
        val RESET_JOB_ID = 68
    }

    fun scheduleReset() {
        info("reset job started")
        val jobComponent = ComponentName(context, ResetJobService::class.java)
        val jobBuilder = JobInfo.Builder(RESET_JOB_ID, jobComponent)
        with(jobBuilder) {
            setMinimumLatency(60 * 60 * 1000L)
        }
        jobScheduler.cancel(RESET_JOB_ID)
        jobScheduler.schedule(jobBuilder.build())
    }

    fun removeJob() {
        info("reset job stopped")
        jobScheduler.cancel(RESET_JOB_ID)
    }
}