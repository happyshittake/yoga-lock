package com.yogabangkit.yogalock.util

import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.info
import org.jetbrains.anko.wtf
import java.lang.Math.abs

object CoordinateUtil : AnkoLogger {
    val RADIUS = 50
    val DIRECTION_UP = 1
    val DIRECTION_RIGHT = 2
    val DIRECTION_DOWN = 3
    val DIRECTION_LEFT = 4
    val DIRECTION_DEFAULT = 5
    val DIRECTION_CENTER = 0

    var startPosX = -1
    var startPosY = -1

    fun getDirection(dragViewX: Int, dragViewY: Int): Int {
        info("DragViewX $dragViewX DragViewY $dragViewY StartViewX $startPosX StartPosY $startPosY")

        val xTmp = abs(dragViewX - startPosX)
        val yTmp = abs(dragViewY - startPosY)
        info("xTmp $xTmp yTmo $yTmp")

        when {
            xTmp <= RADIUS && yTmp <= RADIUS -> return DIRECTION_CENTER
            xTmp <= RADIUS && yTmp > RADIUS -> {
                when {
                    dragViewY > startPosY -> return DIRECTION_UP
                    dragViewY < startPosY -> return DIRECTION_DOWN
                    else -> {
                        wtf("UNKNOWN ERROR in getdirection")
                        return -1
                    }
                }
            }
            xTmp > RADIUS && yTmp <= RADIUS -> {
                when {
                    dragViewX > startPosX -> return DIRECTION_RIGHT
                    dragViewX < startPosX -> return DIRECTION_LEFT
                    else -> {
                        wtf("UNKNOWN ERROR in getdirection")
                        return -1
                    }
                }
            }
            else -> return DIRECTION_CENTER
        }
    }
}