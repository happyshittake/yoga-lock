package com.yogabangkit.yogalock.util

import android.app.job.JobInfo
import android.app.job.JobScheduler
import android.content.ComponentName
import android.content.Context
import com.yogabangkit.yogalock.GpsJobService
import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.info

class GpsJobServiceUtil(val context: Context) : AnkoLogger {

    private val jobScheduler = context.getSystemService(Context.JOB_SCHEDULER_SERVICE) as JobScheduler

    companion object {
        val JOB_ID = 67
    }

    fun startJobService() {
        info("gps job started")
        val jobComponent = ComponentName(context, GpsJobService::class.java)
        val jobBuilder = JobInfo.Builder(JOB_ID, jobComponent)
        with(jobBuilder) {
            setRequiredNetworkType(JobInfo.NETWORK_TYPE_ANY)
            setPeriodic(30 * 1_000)
        }
        jobScheduler.cancel(JOB_ID)
        jobScheduler.schedule(jobBuilder.build())
    }

    fun stopJobService() {
        info("gps job removed")
        jobScheduler.cancel(JOB_ID)
    }
}