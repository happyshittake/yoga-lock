package com.yogabangkit.yogalock.util

import com.marcinmoskala.kotlinpreferences.PreferenceHolder

object UserPreference : PreferenceHolder() {
    var email: String? by bindToPreferenceFieldNullable<String>()
    var guestId: String by bindToPreferenceField("11")
}