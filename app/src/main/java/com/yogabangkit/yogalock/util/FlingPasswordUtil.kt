package com.yogabangkit.yogalock.util

import com.marcinmoskala.kotlinpreferences.PreferenceHolder
import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.debug


object FlingPasswordUtil : PreferenceHolder(), AnkoLogger {
    var password: String by bindToPreferenceField(" up down left right")
    var currentPassword: String by bindToPreferenceField("")
    var hasPassword: Boolean by bindToPreferenceField(false)

    fun setPwd(newPwd: String) {
        password = newPwd
        hasPassword = true
        debug("new password $password")
    }

    fun clearPwd() {
        password = ""
        hasPassword = false
    }

    fun validatePwd() = currentPassword == password

    fun setDefaultPwd() {
        if (!hasPassword) {
            setPwd(" up down left right")
            debug("new password $password")
        }
    }
}