package com.yogabangkit.yogalock.util

import java.io.DataOutputStream

object TerminalUtil {
    fun startUser(id: String) {
        val p = Runtime.getRuntime().exec("su")
        val os = DataOutputStream(p.outputStream)

        with(os) {
            writeBytes("pm enable --user $id com.yogabangkit.yogalock \n")
            flush()
            writeBytes("am start-user $id \n")
            writeBytes("exit\n")
            flush()
        }
    }

    fun stopUser(id: String) {
        val p = Runtime.getRuntime().exec("su")
        val os = DataOutputStream(p.outputStream)

        with(os) {
            writeBytes("am stop-user $id \n")
            writeBytes("exit\n")
            flush()
        }
    }

    fun switchUser(id: String) {
        val p = Runtime.getRuntime().exec("su")
        val os = DataOutputStream(p.outputStream)

        with(os) {
            writeBytes("am switch-user $id \n")
            writeBytes("exit\n")
            flush()
        }
    }

    fun switchToGuest() {
        switchUser(UserPreference.guestId)
    }

    fun hardReset() {
        val p = Runtime.getRuntime().exec("su")
        val os = DataOutputStream(p.outputStream)

        with(os) {
            writeBytes("am broadcast -a android.intent.action.MASTER_CLEAR")
            writeBytes("exit\n")
            flush()
        }
    }

    fun stopAppForUser(id: String) {
        val p = Runtime.getRuntime().exec("su")
        val os = DataOutputStream(p.outputStream)

        with(os) {
            writeBytes("am kill --user $id com.yogabangkit.yogalock")
            writeBytes("exit\n")
            flush()
        }
    }
}