package com.yogabangkit.yogalock.util

import android.app.Activity
import android.view.View
import android.view.WindowManager

class LockUtil(activity: Activity) {
    private var isLocked = false
    private val windowManager = activity.windowManager
    var lockView: View? = null
    private val lockViewLayoutParam = WindowManager.LayoutParams()

    init {
        isLocked = false
        lockViewLayoutParam.width = WindowManager.LayoutParams.MATCH_PARENT
        lockViewLayoutParam.height = WindowManager.LayoutParams.MATCH_PARENT
        lockViewLayoutParam.type = WindowManager.LayoutParams.TYPE_SYSTEM_ERROR
        lockViewLayoutParam.flags =
                WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED or
                        WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD
    }

    companion object Factory {
        fun create(activity: Activity): LockUtil = LockUtil(activity)
    }


    @Synchronized fun lock() {
        if (!isLocked) {
            lockView?.systemUiVisibility =
                    View.SYSTEM_UI_FLAG_IMMERSIVE or
                            View.SYSTEM_UI_FLAG_FULLSCREEN or
                            View.SYSTEM_UI_FLAG_LAYOUT_STABLE or
                            View.SYSTEM_UI_FLAG_HIDE_NAVIGATION or
                            View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION or
                            View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN

            windowManager.addView(lockView!!, lockViewLayoutParam)
        }
        isLocked = true
    }

    @Synchronized fun unlock() {
        if (isLocked) {
            windowManager.removeView(lockView)
        }
        isLocked = false
    }
}