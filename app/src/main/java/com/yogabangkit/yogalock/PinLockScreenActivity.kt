package com.yogabangkit.yogalock

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import com.andrognito.pinlockview.IndicatorDots
import com.andrognito.pinlockview.PinLockListener
import com.andrognito.pinlockview.PinLockView
import com.yogabangkit.yogalock.util.LockUtil
import com.yogabangkit.yogalock.util.TerminalUtil
import com.yogabangkit.yogalock.util.UserPreference
import kotlinx.coroutines.experimental.CommonPool
import kotlinx.coroutines.experimental.delay
import kotlinx.coroutines.experimental.launch
import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.find
import org.jetbrains.anko.info
import org.jetbrains.anko.toast

class PinLockScreenActivity : AppCompatActivity(), AnkoLogger {

    private val PASSWORD = "1234"
    private var retry = 0
    private val MAX_RETRY = 3
    private var pinLockView: PinLockView? = null
    private var indicatorDots: IndicatorDots? = null
    private var lockUtil: LockUtil? = null

    private val pinLockListener = object : PinLockListener {
        override fun onEmpty() {
        }

        override fun onComplete(pin: String?) {
            when (pin) {
                PASSWORD -> {
                    toast("Pin benar, selamat datang kembali...")
                    lockUtil?.unlock()
                    finish()
                }
                else -> {
                    info("wrong password: $pin")
                    pinLockView?.resetPinLockView()
                    if (retry < MAX_RETRY) {
                        retry += 1
                        info("retry: $retry")
                        toast("Wrong pin, you can try ${MAX_RETRY - retry} times...")
                    } else {
                        launch(CommonPool) {
                            TerminalUtil.startUser(UserPreference.guestId)
                            delay(500)
                            TerminalUtil.switchUser(UserPreference.guestId)
                        }
                        finish()
                    }
                }
            }
        }

        override fun onPinChange(pinLength: Int, intermediatePin: String?) {

        }
    }

    companion object {
        var isShown = false
        var instance: PinLockScreenActivity? = null
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        isShown = true
        instance = this

        val view = View.inflate(this, R.layout.activity_pin_lock_screen, null)
        lockUtil = LockUtil.create(this)
        lockUtil?.lockView = view
        lockUtil?.lock()

        pinLockView = view.find(R.id.pinLockView)
        indicatorDots = view.find(R.id.indicatorDots)

        pinLockView?.attachIndicatorDots(indicatorDots)
        pinLockView?.setPinLockListener(pinLockListener)
        pinLockView?.pinLength = PASSWORD.length

        indicatorDots?.indicatorType = IndicatorDots.IndicatorType.FILL_WITH_ANIMATION
    }


    override fun onDestroy() {
        isShown = false
        instance = null
        super.onDestroy()
    }
}
