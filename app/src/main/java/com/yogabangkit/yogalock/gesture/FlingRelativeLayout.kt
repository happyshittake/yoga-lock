package com.yogabangkit.yogalock.gesture

import android.content.Context
import android.graphics.*
import android.util.AttributeSet
import android.view.GestureDetector
import android.view.MotionEvent
import android.view.View
import android.widget.ImageView
import android.widget.RelativeLayout
import com.yogabangkit.yogalock.R
import com.yogabangkit.yogalock.util.FlingPasswordUtil
import kotlinx.coroutines.experimental.CommonPool
import kotlinx.coroutines.experimental.android.UI
import kotlinx.coroutines.experimental.channels.actor
import kotlinx.coroutines.experimental.delay
import kotlinx.coroutines.experimental.launch
import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.debug
import org.jetbrains.anko.find
import org.jetbrains.anko.info

class FlingRelativeLayout : RelativeLayout, AnkoLogger {

    private val gestureDetector: GestureDetector
    private val dragView: Bitmap
    private val initDragViewPos = 3000
    private val DELAY = 10L
    private val DISTANCE = 10
    private val DEALPHAVAL = 10
    private val WHOLEDISTANCE = 20
    private var originDragView: ImageView? = null
    private var dragViewX = initDragViewPos
    private var dragViewY = initDragViewPos
    private var startPosX = -1
    private var startPosY = -1
    private var bmAlpha = 255
    var currentDirection = 5
    var onCompleteListener: (() -> Unit)? = null

    private val gestureActor = actor<Int>(CommonPool) {
        for (msg in channel) {
            when (msg) {
                FlingRelativeLayout.GET_DIRECTION -> {
                    debug("gestureActor GET_DIRECTION")
                    currentDirection = GestureListener.detectDirection
                    savePassword(currentDirection)

                    onCompleteListener?.let {
                        it()
                    }

                    info("currentDirection $currentDirection")
                    launch(CommonPool) {
                        delay(DELAY)
                        flingDrawView()
                        launch(UI) {
                            invalidate()
                        }
                    }
                }
            }
        }
    }

    constructor(context: Context) : super(context)
    constructor(context: Context, attributeSet: AttributeSet) : super(context, attributeSet)
    constructor(context: Context, attributeSet: AttributeSet, defStyle: Int) : super(context, attributeSet, defStyle)

    init {
        val gestureListener = GestureListener()
        info("$gestureListener")
        gestureDetector = GestureDetector(context, gestureListener)
        gestureListener.handler = gestureActor
        dragView = BitmapFactory.decodeResource(resources, R.drawable.ring4)
        info("Relative Layout init")
    }

    companion object {
        val GET_DIRECTION = 10086
    }


    override fun onFinishInflate() {
        super.onFinishInflate()
        debug("onFinishInflate()")
        originDragView = find<ImageView>(R.id.ivOriginDragView)
    }

    override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas)
        debug("===onDraw===")
        initOnDraw(canvas)
    }

    fun savePassword(currentDirection: Int) {
        when (currentDirection) {
            1 -> FlingPasswordUtil.currentPassword += " up"
            2 -> FlingPasswordUtil.currentPassword += " right"
            3 -> FlingPasswordUtil.currentPassword += " down"
            4 -> FlingPasswordUtil.currentPassword += " left"
        }
    }

    private fun initOnDraw(canvas: Canvas?) {
        if (startPosX == -1 || startPosY == -1) {
            startPosX = originDragView!!.left + originDragView!!.width / 2
            startPosY = originDragView!!.top + originDragView!!.height / 2
            debug("init on draw ===> startPosX $startPosX, startPosY $startPosY")
        }

        val paint = Paint()
        paint.alpha = bmAlpha
        canvas?.drawBitmap(dragView, dragViewX.toFloat(), dragViewY.toFloat(), paint)
    }

    private fun flingDrawView() {
        when (currentDirection) {
            0 -> {
                launch(UI) {
                    resetToInit()
                    debug("flingDrawView() currentDirection $currentDirection ...resetToTinit")
                }
            }
            1 -> {
                dragViewY -= DISTANCE
                bmAlpha = if (bmAlpha > 0) bmAlpha - DEALPHAVAL else 0

                if ((startPosY - (dragView.height / 2 + dragViewY)) > WHOLEDISTANCE) {
                    launch(UI) {
                        resetToInit()
                        debug("flingDrawView() currentDirection $currentDirection ...resetToTinit")
                    }
                } else {
                    launch(CommonPool) {
                        delay(DELAY)
                        flingDrawView()
                        debug("flingDrawView() currentDirection $currentDirection ...invalidate")
                        launch(UI) {
                            invalidate()
                            debug("flingDrawView() currentDirection $currentDirection ...invalidate")
                        }
                    }
                }
            }
            2 -> {
                dragViewX += DISTANCE
                bmAlpha = if (bmAlpha > 0) bmAlpha - DEALPHAVAL else 0
                if ((startPosX + (dragView.width / 2 + startPosX)) > WHOLEDISTANCE) {
                    launch(UI) {
                        resetToInit()
                        debug("flingDrawView() currentDirection $currentDirection ...resetToTinit")
                    }
                } else {
                    launch(CommonPool) {
                        delay(DELAY)
                        flingDrawView()
                        launch(UI) {
                            invalidate()
                            debug("flingDrawView() currentDirection $currentDirection ...invalidate")
                        }
                    }
                }
            }
            3 -> {
                dragViewY += DISTANCE
                bmAlpha = if (bmAlpha > 0) bmAlpha - DEALPHAVAL else 0

                if ((dragViewY + (dragView.height / 2 - startPosY)) > WHOLEDISTANCE) {
                    launch(UI) {
                        resetToInit()
                        debug("flingDrawView() currentDirection $currentDirection ...resetToTinit")
                    }
                } else {
                    launch(CommonPool) {
                        delay(DELAY)
                        flingDrawView()
                        launch(UI) {
                            invalidate()
                            debug("flingDrawView() currentDirection $currentDirection ...invalidate")
                        }
                    }
                }
            }
            4 -> {
                dragViewX -= DISTANCE
                bmAlpha = if (bmAlpha > 0) bmAlpha - DEALPHAVAL else 0

                if ((startPosX - (dragView.width / 2 + dragViewX)) > WHOLEDISTANCE) {
                    launch(UI) {
                        resetToInit()
                        debug("flingDrawView() currentDirection $currentDirection ...resetToTinit")
                    }
                } else {
                    launch(CommonPool) {
                        delay(DELAY)
                        flingDrawView()
                        launch(UI) {
                            invalidate()
                            debug("flingDrawView() currentDirection $currentDirection ...invalidate")
                        }
                    }
                }
            }
        }
    }

    override fun onTouchEvent(event: MotionEvent?): Boolean {
        when (event?.action) {
            MotionEvent.ACTION_DOWN -> {
                info("onTouchEvent: ACTION_DOWN")

                if (handleActionDown(event)) {
                    return gestureDetector.onTouchEvent(event)
                } else {
                    resetToInit()
                    return false
                }
            }
        }

        return gestureDetector.onTouchEvent(event)
    }

    private fun handleActionDown(event: MotionEvent): Boolean {
        val rect = Rect()
        originDragView?.getHitRect(rect)
        val x = event.x
        val y = event.y

        val isInRect = rect.contains(x.toInt(), y.toInt())

        if (isInRect) {
            originDragView?.visibility = View.INVISIBLE
            dragViewX = startPosX - dragView.width / 2
            dragViewY = startPosY - dragView.height / 2
        }

        info("handleActionDown: isInRect $isInRect")
        return isInRect
    }

    private fun resetToInit() {
        dragViewX = initDragViewPos
        dragViewY = initDragViewPos

        originDragView?.visibility = View.VISIBLE

        currentDirection = 0

    }
}