package com.yogabangkit.yogalock.gesture

import android.view.GestureDetector
import android.view.MotionEvent
import kotlinx.coroutines.experimental.channels.ActorJob
import kotlinx.coroutines.experimental.runBlocking
import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.info
import java.lang.Math.abs

class GestureListener : GestureDetector.SimpleOnGestureListener(), AnkoLogger {
    private val minFlingDistance = 50

    private val DIRECTION_UP = 1
    private val DIRECTION_RIGHT = 2
    private val DIRECTION_DOWN = 3
    private val DIRECTION_LEFT = 4
    private val DIRECTION_CENTER = 0

    var handler: ActorJob<Int>? = null

    companion object {
        var detectDirection = -1
    }

    override fun onDown(e: MotionEvent?): Boolean {
        return true
    }

    override fun onFling(e1: MotionEvent?, e2: MotionEvent?, velocityX: Float, velocityY: Float): Boolean {
        info("GestureListener: onFling()")
        handleFlingDirection(e1, e2, velocityX, velocityY)
        return super.onFling(e1, e2, velocityX, velocityY)
    }

    private fun handleFlingDirection(e1: MotionEvent?, e2: MotionEvent?, velocityX: Float, velocityY: Float) = runBlocking {
        val e1x = e1?.x
        val e1y = e1?.y
        val e2x = e2?.x
        val e2y = e2?.y

        info("e1x: $e1x e1y: $e1y e2x: $e2x e2y: $e2y")

        when {
            abs(velocityX) < abs(velocityY) -> {
                if (velocityY < 0f) {
                    detectDirection = DIRECTION_UP
                } else {
                    detectDirection = DIRECTION_DOWN
                }
            }
            abs(velocityX) > abs(velocityY) -> {
                if (velocityX < 0) {
                    detectDirection = DIRECTION_LEFT
                } else {
                    detectDirection = DIRECTION_RIGHT
                }
            }
            else -> detectDirection = DIRECTION_CENTER
        }

        info("detectDirection: $detectDirection")

        handler?.offer(FlingRelativeLayout.GET_DIRECTION)
    }
}