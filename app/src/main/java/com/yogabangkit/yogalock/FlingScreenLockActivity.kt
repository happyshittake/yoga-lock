package com.yogabangkit.yogalock

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.Button
import android.widget.TextView
import com.yogabangkit.yogalock.gesture.FlingRelativeLayout
import com.yogabangkit.yogalock.util.FlingPasswordUtil
import com.yogabangkit.yogalock.util.LockUtil
import com.yogabangkit.yogalock.util.TerminalUtil
import com.yogabangkit.yogalock.util.UserPreference
import kotlinx.coroutines.experimental.CommonPool
import kotlinx.coroutines.experimental.android.UI
import kotlinx.coroutines.experimental.delay
import kotlinx.coroutines.experimental.launch
import org.jetbrains.anko.*

class FlingScreenLockActivity : AppCompatActivity(), AnkoLogger {
    private var lockView: View? = null
    private var lockUtil: LockUtil? = null

    private var flingRelativeLayout: FlingRelativeLayout? = null
    private var btnConfirm: Button? = null
    private var btnClear: Button? = null
    private var topInfo: TextView? = null
    private var passwordReveal: TextView? = null
    private var retry = 0
    private val MAX_RETRY = 3

    companion object {
        var isShown: Boolean = false
        var instance: FlingScreenLockActivity? = null
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        debug("FlingLockScreenActivity > onCreate")
        instance = this
        isShown = true
        lockView = View.inflate(this, R.layout.activity_fling_screen_lock, null)
        lockUtil = LockUtil.create(this)
        lockUtil?.lockView = lockView
        lockUtil?.lock()

        flingRelativeLayout = lockView?.find<FlingRelativeLayout>(R.id.flingRelativeLayout)
        btnConfirm = lockView?.find<Button>(R.id.btnConfirm)
        btnClear = lockView?.find<Button>(R.id.btnClear)
        topInfo = lockView?.find<TextView>(R.id.topInfo)
        passwordReveal = lockView?.find<TextView>(R.id.passwordReveal)

        passwordReveal?.text = ""

        initFlingThings()
    }

    private fun initFlingThings() {
        btnConfirm?.visibility = View.VISIBLE
        btnClear?.visibility = View.VISIBLE
        topInfo?.text = "please enter gestures to unlock..."
        flingRelativeLayout?.onCompleteListener = {
            if (FlingPasswordUtil.currentPassword != "") {
                launch(UI) {
                    passwordReveal?.text = FlingPasswordUtil.currentPassword
                }
            }
        }

        btnConfirm?.setOnClickListener {
            if (FlingPasswordUtil.validatePwd()) {
                FlingPasswordUtil.currentPassword = ""
                passwordReveal?.text = ""
                lockUtil?.unlock()
                startActivity(intentFor<PinLockScreenActivity>().newTask())
                finish()
            } else {
                info("wrong password : ${FlingPasswordUtil.currentPassword}")
                FlingPasswordUtil.currentPassword = ""
                passwordReveal?.text = ""
                if (retry < MAX_RETRY) {
                    retry += 1
                    info("retry: $retry")
                    toast("Password error, you can try ${MAX_RETRY - retry} times")
                } else {
                    lockUtil?.unlock()
                    launch(CommonPool) {
                        TerminalUtil.startUser(UserPreference.guestId)
                        delay(500)
                        TerminalUtil.switchUser(UserPreference.guestId)
                    }
                    finish()
                }
            }
        }

        btnClear?.setOnClickListener {
            FlingPasswordUtil.currentPassword = ""
            passwordReveal?.text = ""
            toast("Password cleared")
        }
    }

    override fun onDestroy() {
        isShown = false
        FlingPasswordUtil.currentPassword = ""
        instance = null
        super.onDestroy()
    }
}
