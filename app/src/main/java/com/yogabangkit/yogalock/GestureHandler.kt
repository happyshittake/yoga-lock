package com.yogabangkit.yogalock

import android.os.Handler
import android.os.Message
import com.yogabangkit.yogalock.gesture.FlingRelativeLayout
import com.yogabangkit.yogalock.gesture.GestureListener
import kotlinx.coroutines.experimental.runBlocking
import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.coroutines.experimental.Ref
import org.jetbrains.anko.debug


class GestureHandler : Handler(), AnkoLogger {
    lateinit var flingLayoutRef: Ref<FlingRelativeLayout>

    override fun handleMessage(msg: Message?) = runBlocking {
        when(msg?.what) {
            FlingRelativeLayout.GET_DIRECTION -> {
                debug("GestureHandler GET_DRIECTION")
                flingLayoutRef().currentDirection = GestureListener.detectDirection
                flingLayoutRef().savePassword(flingLayoutRef().currentDirection)

            }
        }
    }
}