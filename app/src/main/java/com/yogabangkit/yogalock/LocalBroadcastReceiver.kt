package com.yogabangkit.yogalock

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.os.Process
import com.yogabangkit.yogalock.util.GpsJobServiceUtil
import com.yogabangkit.yogalock.util.ResetUtil
import com.yogabangkit.yogalock.util.TerminalUtil
import com.yogabangkit.yogalock.util.UserPreference
import org.jetbrains.anko.*

class LocalBroadcastReceiver : BroadcastReceiver(), AnkoLogger {
    companion object {
        val START_JOB_INTENT = "com.yogabangkit.yogalock.intent.startjobs"
    }

    override fun onReceive(context: Context, intent: Intent) {
        val action = intent.action
        val gpsJobUtil = GpsJobServiceUtil(context)
        val resetJobUtil = ResetUtil(context)
        info("the intent action is $action")

        when (action) {
            Intent.ACTION_SCREEN_OFF -> {
                if (isAdmin(context)) {
                    tearDownAllLockScreen()
                    showLockScreen(context)
                }
            }
            Intent.ACTION_SCREEN_ON -> {
            }
            START_JOB_INTENT -> {
                info("BROADCAST_RECEIVED")
                if (!isAdmin(context)) {
                    gpsJobUtil.startJobService()
                    resetJobUtil.scheduleReset()
                }
            }
            Intent.ACTION_USER_FOREGROUND -> {
                gpsJobUtil.stopJobService()
                resetJobUtil.removeJob()
                TerminalUtil.stopUser(UserPreference.guestId)
            }
            Intent.ACTION_BOOT_COMPLETED -> {
                if (!isAdmin(context)) {
                    gpsJobUtil.startJobService()
                    resetJobUtil.scheduleReset()
                } else {
                    showLockScreen(context)
                    context.startService(context.intentFor<LocalService>())
                }
            }
        }
    }

    private fun isAdmin(context: Context): Boolean {
        val uh = Process.myUserHandle()
        val um = context.userManager
        info("user android : ${um.getSerialNumberForUser(uh)}")
        return um.getSerialNumberForUser(uh) == 0L
    }

    private fun showLockScreen(context: Context) {
        if (!MainActivity.isShown) {
            MainActivity.isShown = true
            context.startActivity(
                    context.intentFor<MainActivity>()
                            .newTask()
            )
        }
    }

    private fun tearDownAllLockScreen() {
        MainActivity.isShown = false
        MainActivity.instance?.let {
            it.finish()
        }
        FlingScreenLockActivity.isShown = false
        FlingScreenLockActivity.instance?.let {
            it.finish()
        }
        PinLockScreenActivity.isShown = false
        PinLockScreenActivity.instance?.let {
            it.finish()
        }
    }
}