package com.yogabangkit.yogalock.email

import korex.activation.DataHandler
import korex.mail.*
import korex.mail.internet.InternetAddress
import korex.mail.internet.MimeMessage
import korex.mail.util.ByteArrayDataSource
import java.security.Security
import java.util.*

class GmailSender(val user: String, val password: String) : Authenticator() {
    private val session: Session
    private val mailhost = "smtp.gmail.com"

    companion object {
        init {
            Security.addProvider(JSSEProvider())
        }
    }

    init {
        val props = Properties()
        props.setProperty("mail.transport.protocol", "smtp")
        props.setProperty("mail.host", mailhost)
        props.put("mail.smtp.auth", "true")
        props.put("mail.smtp.port", "465")
        props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory")
        props.put("mail.smtp.socketFactory.fallback", "false")
        props.setProperty("mail.smtp.quitwait", "false")

        session = Session.getDefaultInstance(props, this)
    }

    override fun getPasswordAuthentication() = PasswordAuthentication(user, password)

    suspend fun sendMail(subject: String, body: String, sender: String, recipients: String) {
        val message = MimeMessage(session)
        val handler = DataHandler(ByteArrayDataSource(body, "text/plain"))
        message.sender = InternetAddress(sender)
        message.subject = subject
        message.dataHandler = handler
        if (recipients.indexOf(',') > 0) {
            message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(recipients))
        } else {
            message.setRecipient(Message.RecipientType.TO, InternetAddress(recipients))
        }

        Transport.send(message)
    }
}